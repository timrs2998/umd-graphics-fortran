module scene_module_spec
    use vector_module
    use scene_module
    use light_module
    use shape_module
    use sphere_module

    contains
        subroutine new_scene_spec
            type(Scene) :: s
            s = Scene()

            if (size(s%lights) /= 3) then
                write(*, *) 'There must be 3 lights'
                stop -1
            endif

            select type(sphr => s%shapes(1)%s)
            type is (Sphere)
                return
            class default
                write(*, *) 'Must be a sphere'
                stop -1
            end select
        end subroutine
end module

program scene_spec
    use scene_module_spec
    call new_scene_spec
end program
