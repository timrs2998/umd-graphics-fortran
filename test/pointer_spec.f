module pointer_module_spec
    use vector_module
    use hit_module
    private change_pointer, change_hit_pointer

    contains
        subroutine change_pointer_spec
            real, pointer :: ptr => null()
            allocate(ptr)
                ptr = 5.0
                if (abs(ptr - 5.0) > 1e-3) then
                    stop 'ptr should be 5.0'
                endif
            deallocate(ptr)
        end subroutine

        subroutine change_pointer_in_func_spec
            real, pointer :: ptr => null()
            logical :: temp
            allocate(ptr)
                ptr = 5.0
                temp = change_pointer(ptr)
                if (abs(ptr - 6.0) > 1e-3) then
                    stop 'ptr should be 6.0'
                endif
            deallocate(ptr)
        end subroutine

        logical function change_pointer(ptr)
            real, pointer :: ptr
            ptr = 6.0
            change_pointer = .true.
        end function

        subroutine change_hit_pointer_in_func_spec
            type(Hit), pointer :: hit_ptr => null()
            logical :: temp
            allocate(hit_ptr)
                hit_ptr = Hit()
                temp = change_hit_pointer(hit_ptr)
                if (abs(hit_ptr%t - 4.0) > 1e-3) then
                    stop 'ptr%t should be 4.0'
                endif
            deallocate(hit_ptr)
        end subroutine

        logical function change_hit_pointer(ptr)
            type(Hit), pointer :: ptr
            call ptr%set(                          &
                n= (/ 1.0, 1.0, 1.0 /),            &
                view= (/ 2.0, 2.0, 2.0 /),         &
                intersection= (/ 3.0, 3.0, 3.0 /), &
                t= 4.0                             &
            )
            change_hit_pointer = .true.
        end function
end module

program pointer_spec
    use pointer_module_spec
    call change_pointer_spec
    call change_pointer_in_func_spec
end program
