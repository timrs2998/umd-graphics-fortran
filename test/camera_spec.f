module camera_module_spec
    use camera_module

    contains
        subroutine new_camera_spec
            type(Camera) :: cam

            cam = Camera(                          &
                position= (/ 0.0, 3.0, 4.0 /),     &
                view_dir= (/ 0.0, -1.5, -3.0 /),   &
                focal_length= 0.4,                 &
                image_plane_width= 0.5             &
            )

            if (.not. all(abs(cam%position - (/ 0, 3, 4 /)) < 1e-3)) then
                write(*, *) 'cam%position'
                stop -1
            else if (.not. all(abs(cam%look_at - (/ 0.0, 1.5, 1.0 /)) < 1e-3)) then
                write(*, *) 'cam%look_at'
                stop -1
            else if (.not. all(abs(cam%b%u - (/ 1, -0, 0 /)) < 1e-3)) then
                write(*, *) 'cam%b%u'
                stop -1
            else if (.not. all(abs(cam%b%v - (/ -0.0, -0.894427299, 0.447213650 /)) < 1e-3)) then
                write(*, *) 'cam%b%v'
                stop -1
            else if (.not. all(abs(cam%b%w - (/ -0.0, 0.447213650, 0.894427299 /)) < 1e-3)) then
                write(*, *) 'cam%b%w'
                stop -1
            endif

            ! cam%focal_length
            if (abs(cam%focal_length - 0.4) > 1e-3) then
                write(*, *) 'cam%focal_length'
                stop -1
            endif

            ! cam%image_plane_width
            if (abs(cam%image_plane_width - 0.5) > 1e-3) then
                write(*, *) 'cam%image_plane_width'
                stop -1
            endif

            ! cam%image_plane_height
            if (abs(cam%image_plane_height - 0.5) > 1e-3) then
                write(*, *) 'cam%image_plane_height'
                stop -1
            endif

            ! cam%nx
            if (abs(cam%nx - 800.0) > 1e-3) then
                write(*, *) 'cam%nx'
                stop -1
            endif

            ! cam%ny
            if (abs(cam%ny - 800.0) > 1e-3) then
                write(*, *) 'cam%ny'
                stop -1
            endif
        end subroutine

        subroutine compute_ray_spec
            type(Camera) :: cam
            type(Ray) :: r

            cam = Camera(                          &
                position= (/ 0.0, 3.0, 4.0 /),     &
                view_dir= (/ 0.0, -1.5, -3.0 /),   &
                focal_length= 0.4,                 &
                image_plane_width= 0.5             &
            )

            r = cam%compute_ray(50, 50)

            ! r%origin
            if (.not. all(abs(r%origin - (/ 0, 3, 4 /)) < 1e-3)) then
                write(*, *) 'r%origin'
                stop -1
            endif

            ! TODO: if nx or ny change, this test will break
            ! r%direction
            if (.not. all(abs(r%direction - (/ -0.218750000, 1.67705268E-02, -0.455598891 /)) < 1e-3)) then
                write(*, *) 'r%direction', r%direction
                stop -1
            endif
        end subroutine
end module

program camera_spec
    use camera_module_spec
    call new_camera_spec
    call compute_ray_spec
end program
