module parent_module
    type, abstract :: Shape
        real :: parent_member
        contains
            procedure(get_number_interface), deferred :: get_number
    end type

    interface
        integer function get_number_interface(self)
            import
            class(Shape), intent(in) :: self
        end function
    end interface
end module
