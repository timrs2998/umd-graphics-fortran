module child_module
    use parent_module

    type, extends(Shape) :: Sphere
        real :: child_member
        contains
            procedure :: get_number
    end type

    contains
        integer function get_number(self)
            class(Sphere), intent(in) :: self
            get_number = self%child_member
        end function
end module
