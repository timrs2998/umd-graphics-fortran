module vector_module_spec
    use vector_module

    contains
        subroutine dot_spec
            real, dimension(3) :: v0 = (/ 1, 2, 3 /)
            real, dimension(3) :: v1 = (/ 9, 34, 1/)
            real :: result

            result = dot_product(v0, v1)

            if (abs(result - 80) > 1e-3) then
                write(*, *) 'result should be 80'
                stop -1
            endif
        end subroutine

        subroutine mult_scalar_spec
            real, dimension(3) :: v0 = (/ 1, 2, 3 /)
            real, dimension(3) :: a = 2
            real, dimension(3) :: result

            result = v0 * a

            if (.not. all(abs(result - (/ 2, 4, 6 /)) < 1e-3)) then
                write(*, *) 'mult_scalar_spec failed'
                stop -1
            endif
        end subroutine

        subroutine mult_vector_spec
            real, dimension(3) :: v0 = (/ 1, 2, 3 /)
            real, dimension(3) :: v1 = (/ 2, 4, 6 /)
            real, dimension(3) :: result

            result = v0 * v1

            if (.not. all(abs(result - (/ 2, 8, 18 /)) < 1e-3)) then
                write(*, *) 'mult_vector_spec failed'
                stop -1
            endif
        end subroutine

        subroutine cross_spec
            real, dimension(3) :: v0 = (/ 1, 2, 3 /)
            real, dimension(3) :: v1 = (/ 9, 34, 1/)
            real, dimension(3) :: result

            result = cross_product(v0, v1)

            if (.not. all(abs(result - (/ -100, 26, 16 /)) < 1e-3)) then
                write(*, *) 'cross_spec failed'
                stop -1
            endif
        end subroutine

        subroutine clamp_spec
            real, dimension(3) :: a = (/ -1.5, 2.0, 0.5 /)
            real, dimension(3) :: result

            result = clamp(a)

            if (.not. all(abs(result - (/ 0.0, 1.0, 0.5 /)) < 1e-3)) then
                write(*, *) 'clamp_spec failed'
                stop -1
            endif
        end subroutine

        subroutine length_spec
            real, dimension(3) :: a = (/ 2, 2, 1/)
            real :: result

            result = length(a)

            if (abs(result - 3.0) > 1e-3) then
                write(*, *) 'result should be 3.0'
                stop -1
            endif
        end subroutine

        subroutine normal_spec
            real, dimension(3) :: a = (/ 2, 2, 1 /)
            real, dimension(3) :: result

            result = normal(a)

            if (.not. all(abs(result - (/ 2.0 / 3.0, 2.0 / 3.0, 1.0 / 3.0 /)) < 1e-3)) then
                write(*, *) 'normal_spec failed'
                stop -1
            endif
        end subroutine
end module

program vector_spec
    use vector_module
    use vector_module_spec

    call dot_spec
    call mult_scalar_spec
    call mult_vector_spec
    call cross_spec
    call clamp_spec
    call length_spec
    call normal_spec
end program
