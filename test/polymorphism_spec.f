module polymorphism_module_spec
    use parent_module
    use child_module

    contains
        subroutine polymorphism_get_member_spec
            class(Shape), pointer :: shp => null()
            type(Sphere), pointer :: sphr => null()
            allocate(sphr)
                sphr = Sphere(parent_member= 5.0, child_member= 6.0)
                shp => sphr
                if (abs(shp%parent_member - 5.0) > 1e-3) then
                    write(*, *) 'shp%parent_member'
                    stop -1
                elseif (abs(sphr%child_member - 6.0) > 1e-3) then
                    write(*, *) 'shp%child_member'
                    stop -1
                elseif (abs(shp%get_number() - 6.0) > 1e-3) then
                    write(*, *) 'shp%get_number()'
                    stop -1
                endif
            deallocate(sphr)
        end subroutine
end module

program polymorphism_spec
    use polymorphism_module_spec
    call polymorphism_get_member_spec
end program
