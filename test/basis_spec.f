module basis_module_spec
    use vector_module
    use basis_module

    contains
        subroutine two_vectors_spec
            real, dimension(3) :: viewDir = (/ 0, 0, -1 /)
            real, dimension(3) :: b = (/ 0, 1, 0 /)
            type(Basis) :: result

            result = Basis(viewDir, b)

            if (.not. all(abs(result%u - (/ 1, -0, -0 /)) < 1e-3)) then
                write(*, *) 'result%u'
                stop -1
            elseif (.not. all(abs(result%v - (/ -0, -1, -0 /)) < 1e-3)) then
                write(*, *) 'result%v', result%v
                stop -1
            elseif (.not. all(abs(result%w - (/ -0, -0, 1 /)) < 1e-3)) then
                write(*, *) 'result%w'
                stop -1
            endif
        end subroutine

        subroutine colinear_spec
            real, dimension(3) :: a = (/ 1, 0, 0 /)
            real, dimension(3) :: b = (/ 10, 0, 0 /)
            logical result

            result = are_colinear(a, b)

            if (.not. result) then
                write(*, *) 'result should be colinear'
                stop -1
            endif
        end subroutine

        subroutine colinear_2_spec
            real, dimension(3) :: a = (/ 1.0, 0.0, 0.0 /)
            real, dimension(3) :: b = (/ 1.0, 1e-2, 0.0 /)
            logical result

            result = are_colinear(a, b)

            if (result) then
                write(*, *) 'result should not be colinear'
                stop -1
            endif
        end subroutine
end module

program basis_spec
    use basis_module_spec
    call two_vectors_spec
    call colinear_spec
    call colinear_2_spec
end program
