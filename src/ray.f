module ray_module
    type Ray
        real, dimension(3) :: origin
        real, dimension(3) :: direction
    end type
end module
