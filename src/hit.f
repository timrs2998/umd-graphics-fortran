module hit_module
    ! use shape_module
    use vector_module

    type Hit
        real, dimension(3) :: n
        real, dimension(3) :: v
        real, dimension(3) :: intersection
        real, allocatable  :: t
        ! class(Shape), pointer :: s
        contains
            ! final :: destructor
            procedure :: set
    end type

    interface Hit
        procedure new_hit
    end interface

    contains
        function new_hit()
            type(Hit) :: new_hit
            new_hit%n = (/ 0, 0, 0 /)
            new_hit%v = (/ 0, 0, 0 /)
            new_hit%intersection = (/ 0, 0, 0 /)
            allocate(new_hit%t)
            new_hit%t = 0
        end function

        ! subroutine destructor(self)
        !     type(Hit) :: self
        !     write(*, *) 'LOG: Hit.destructor()'
        !     if (allocated(self%t)) then
        !         deallocate(self%t)
        !     endif
        ! end subroutine

        subroutine set(self, n, view, intersection, t)
            class(Hit) :: self
            real, dimension(3), intent(in) :: n
            real, dimension(3), intent(in) :: view
            real, dimension(3), intent(in) :: intersection
            real, intent(in) :: t

            self%n = normal(n)
            self%v = normal(view)
            self%intersection = intersection
            self%t = t
        end subroutine
end module
