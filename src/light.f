module light_module
    type Light
        real, dimension(3) :: position
        real, dimension(3) :: intensity
    end type
end module
