module scene_module
    use camera_module
    use light_module
    use hit_module
    use shape_module
    use sphere_module
    use shader_module
    use vector_module

    ! http://stackoverflow.com/a/8901228/1509183
    type, private :: ShapePointer
        class(Shape), pointer :: s
    end type

    interface ShapePointer
        procedure new_shape_ptr
    end interface

    type Scene
        type(Camera) :: cam
        type(Light), dimension(:), allocatable :: lights
        type(ShapePointer), dimension(:), allocatable :: shapes
        contains
            procedure :: get_color
            final :: destructor
    end type

    interface Scene
        module procedure new_scene
    end interface

contains
    function new_shape_ptr()
        type(ShapePointer) :: new_shape_ptr
        new_shape_ptr%s => null()
    end function

    ! simpleTwoSpheres.xml written inline instead of xml parsing
    function new_scene()
        type(Scene) :: new_scene
        type(Sphere), pointer :: sphr1 => null()
        type(Sphere), pointer :: sphr2 => null()

        ! Allocate new array
        allocate(new_scene%shapes(2))

        ! Box sphere into pointer type and add to shapes
        new_scene%shapes(1) = ShapePointer()
        allocate(sphr1)
        sphr1 = Sphere(                    &
            shdr= Shader(),                &
            center= (/ -1.2, 1.0, -3.0 /), &
            radius= 1                      &
        )
        new_scene%shapes(1)%s => sphr1

        new_scene%shapes(2) = ShapePointer()
        allocate(sphr2)
        sphr2 = Sphere(                    &
            shdr = Shader(),               &
            center= (/ 1.2, 1.1, -4.0 /),  &
            radius= 1.1                    &
        )
        new_scene%shapes(2)%s => sphr2

        ! Camera
        new_scene%cam = Camera(                &
            position= (/ 0.0, 3.0, 4.0 /),     &
            view_dir= (/ 0.0, -1.5, -3.0 /),   &
            focal_length= 0.4,                 &
            image_plane_width= 0.5             &
        )

        ! Lights
        allocate(new_scene%lights(3))
        new_scene%lights(1) = Light(           &
            position= (/ 10.0, 10.0, 1.0 /),   &
            intensity= (/ 1.0, 1.0, 1.0 /)     &
        )
        new_scene%lights(2) = Light(           &
            position= (/ 10.0, 8.0, 1.0 /),    &
            intensity= (/ 0.8, 0.8, 0.8 /)     &
        )
        new_scene%lights(3) = Light(           &
            position= (/ 1.0, 3.0, 8.0 /),     &
            intensity= (/ 0.25, 0.25, 0.25 /)  &
        )
    end function

    subroutine destructor(this)
        type(Scene) :: this
        ! integer :: i
        write(*, *) 'LOG: Scene.destructor()'
        ! Shapes
        !if (allocated(this%shapes)) then
        !    do i = 1, size(this%shapes)
        !        if (allocated(this%shapes(i)%s)) then
        !            deallocate(this%shapes(i)%s)
        !        endif
        !    enddo
        !    deallocate(this%shapes)
        !endif

        ! Lights
        if (allocated(this%lights)) then
            deallocate(this%lights)
        endif
    end subroutine

    function get_color(self, r)
        class(Scene), intent(in) :: self
        type(Ray), intent(in)   :: r
        real, dimension(3) :: get_color

        integer :: i
        type(Hit), pointer :: h_pointer => null()
        real, parameter :: tmin = 1
        real, pointer :: tmax_pointer => null()
        logical :: intersected = .false.

        type(Shader) :: shdr ! TODO: get self from hit%shape
        shdr = Shader()

        allocate(h_pointer)
        h_pointer = Hit()
        allocate(tmax_pointer)
        tmax_pointer = 99999.0

        ! for each shape, perform ray--shape intersection testing
        intersected = .false.        ! TODO: why does self line fix my program!?
        do i = 1, size(self%shapes)
            if (self%shapes(i)%s%closestHit(r, tmin, tmax_pointer, h_pointer)) then
                intersected = .true.
            endif
        enddo

        ! apply shader or get background color
        if (intersected) then
            if (abs(tmax_pointer - 99999.0) < 1e-3) then ! Important debugging code
                write(*, *) tmax_pointer
                stop 'ERROR! tmax never changed after hit'
            endif

            get_color = clamp(shdr%apply(h_pointer))
        else
            get_color = (/ 0.15, 0.15, 0.15 /)
        endif
        deallocate(h_pointer)
        deallocate(tmax_pointer)
    end function
end module
