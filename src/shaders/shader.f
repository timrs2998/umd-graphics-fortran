module shader_module
    use hit_module
    use vector_module
    public :: Shader

    type Shader
        contains
            procedure :: apply
    end type

    contains
        ! TODO: support more than normal vector shading
        function apply(self, h)
            class(Shader), intent(in) :: self
            type(Hit), pointer, intent(in)     :: h
            real, dimension(3) :: apply
            apply = h%n * 0.5 + 0.5
        end function
end module
