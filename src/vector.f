module vector_module

contains
    ! We'll rely on these intrinsic functions:
    ! https://gcc.gnu.org/onlinedocs/gfortran/DOT_005fPRODUCT.html#DOT_005fPRODUCT

    pure elemental function clamp(a)
        real, intent(in) :: a
        real             :: clamp
        clamp = merge(1.0, merge(0.0, a, a < 0.0), a > 1.0)
    end function

    pure real function length(a)
        real, intent(in) :: a(:)
        length = sqrt(a(1)**2 + a(2)**2 + a(3)**2)
    end function

    ! Source: http://rosettacode.org/wiki/Vector_products#Fortran
    pure function cross_product(a, b)
        real, dimension(3) :: cross_product
        real, dimension(3), intent(in) :: a, b

        cross_product(1) = a(2)*b(3) - a(3)*b(2)
        cross_product(2) = a(3)*b(1) - a(1)*b(3)
        cross_product(3) = a(1)*b(2) - b(1)*a(2)
    end function

    function normal(a)
        real, dimension(3) :: normal
        real, dimension(3), intent(in) :: a
        real :: l
        l = length(a)
        if (l == 0.0) then
            stop 'Division by zero in normal()'
        end if
        normal = a / l
    end function
end module
