module basis_module
    use vector_module

    type Basis
        real, dimension(3) :: u
        real, dimension(3) :: v
        real, dimension(3) :: w
    end type

    interface Basis
        procedure new_basis
    end interface

    real, private, parameter :: EPSILON = 1e-3

    contains
        function new_basis(a, b)
            type(Basis) :: new_basis
            real, dimension(3), intent(in) :: a
            real, dimension(3), intent(in) :: b

            new_basis%w = normal(-a)
            new_basis%u = cross_product(new_basis%w, b)
            if (are_colinear(new_basis%w, b)) then
                new_basis%u = cross_product(new_basis%w, b + EPSILON)
            endif
            new_basis%u = normal(new_basis%u)

            new_basis%v = normal(cross_product(new_basis%w, new_basis%u))
            new_basis%u = -new_basis%u
        end function

        logical function are_colinear(a_unsafe, b_unsafe)
            real, dimension(3) :: a_unsafe
            real, dimension(3) :: b_unsafe
            real, dimension(3) :: a
            real, dimension(3) :: b
            a = normal(a_unsafe)
            b = normal(b_unsafe)
            are_colinear = abs(a(1) - b(1)) < EPSILON .and. &
                abs(a(2) - b(2)) < EPSILON .and.            &
                abs(a(3) - b(3)) < EPSILON
        end function
end module
