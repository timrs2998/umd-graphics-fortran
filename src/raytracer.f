program raytracer
    use camera_module
    use vector_module
    use ray_module
    use shape_module
    use sphere_module
    use scene_module

    integer :: i, j
    type(Ray) :: r
    type(Scene) :: s
    real, dimension(3) :: color
    character(12) :: color_string

    ! Construct scene
    s = Scene()

    ! File IO with PPM format for simplicity:
    ! https://en.wikipedia.org/wiki/Netpbm_format#PPM_example
    open(unit=1, file='image_output.ppm', status='replace', access='stream')
        write(1) 'P3' // char(10)
        write(1) '800 800' // char(10)
        write(1) '255' // char(10)

        ! For each pixel
        do i = 1,800
            do j = 1,800
                r = s%cam%compute_ray(i, j)
                color = s%get_color(r)
                color = color * 255

                ! Debug along the diagonal
                ! if (i == j .and. mod(i, 10) == 0) then
                !     write(*, *) i, j, r%origin, r%direction
                ! endif

                write(color_string, '(i3)') int(color(1))
                write(1) color_string

                write(color_string, '(i3)') int(color(2))
                write(1) color_string

                write(color_string, '(i3)') int(color(3))
                write(1) color_string
            enddo
            write(1) char(10)
        enddo

        write(*, *) 'Wrote to image_output.ppm'
    close(unit=1)
end program
