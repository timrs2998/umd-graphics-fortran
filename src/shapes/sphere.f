module sphere_module
    use hit_module
    use shape_module
    use ray_module
    use vector_module

    private
    public :: Sphere

    type, extends(Shape) :: Sphere
        real, dimension(3) :: center
        real :: radius
        contains
            procedure :: closestHit
    end type

    contains
        logical function closestHit(self, r, tmin, tmax, h)
            class(Sphere), intent(in) :: self
            type(Ray), intent(in) :: r
            real, intent(in) :: tmin
            real, pointer :: tmax
            type(Hit), pointer, intent(in) :: h

            real, dimension(3) :: center
            real :: radius
            real, dimension(3) :: ro, rd
            real :: a, b, c, discriminant, t0, t1, temp
            real, dimension(3) :: intersection, n, view

            ro = r%origin
            rd = r%direction
            center = self%center
            radius = self%radius

            a = dot_product(rd, rd)
            b = dot_product(rd *  2.0, ro - center)
            c = dot_product(ro - center, ro - center) - radius * radius

            discriminant = b * b - 4.0 * a * c
            if (discriminant < 0.0) then
                closestHit = .false.
            else
                t0 = (-b + sqrt(discriminant)) / (2.0 * a)
                t1 = (-b - sqrt(discriminant)) / (2.0 * a)
                if (t0 > t1) then
                    temp = t0
                    t0 = t1
                    t1 = temp
                endif

                if (tmin < t0 .and. t0 < tmax) then        ! Case 1: entering sphere
                    intersection = ro + (rd * t0)
                    n = intersection - self%center
                    view = ro - intersection
                    call h%set(n, view, intersection, t0)
                    tmax = t0 ! CHANGE tmax BY REFERENCE

                    closestHit = .true.
                elseif (tmin < t1 .and. t1 < tmax) then   ! Case 2: exiting sphere
                    intersection = ro + (rd * t1)
                    n = intersection - self%center
                    view = ro - intersection
                    call h%set(n, view, intersection, t1)
                    tmax = t1 ! CHANGE tmax BY REFERENCE

                    closestHit = .true.
                else
                    closestHit = .false.
                endif
            endif
        end function
end module
