! http://stackoverflow.com/a/25413107/1509183
! http://stackoverflow.com/q/18653974/1509183

module shape_module
    use hit_module
    use ray_module
    use vector_module
    use shader_module

    type, abstract :: Shape
        type(Shader) :: shdr
        contains
            procedure(closestHitInterface), deferred :: closestHit
    end type

    interface
        logical function closestHitInterface(self, r, tmin, tmax, h)
            import
            class(Shape), intent(in) :: self
            type(Ray), intent(in) :: r
            real, intent(in) :: tmin
            real, pointer :: tmax
            type(Hit), pointer, intent(in) :: h
        end function
    end interface
end module
