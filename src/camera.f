module camera_module
    use basis_module
    use ray_module

    type Camera
        real, dimension(3) :: position, look_at
        type(Basis) :: b
        real :: focal_length
        real :: image_plane_height, image_plane_width
        real :: nx, ny
        contains
            procedure :: compute_ray
    end type

    interface Camera
        module procedure new_camera
    end interface

    contains
        function new_camera(position, view_dir, focal_length, image_plane_width)
            type(Camera) :: new_camera
            real, dimension(3), intent(in) :: position
            real, dimension(3), intent(in) :: view_dir
            real, intent(in) :: focal_length
            real, intent(in) :: image_plane_width

            new_camera%position = position
            new_camera%look_at = position + view_dir
            new_camera%focal_length = focal_length
            new_camera%image_plane_width = image_plane_width
            new_camera%nx = 800.0 ! TODO: hardcoded values must match raytracer.f
            new_camera%ny = 800.0
            new_camera%image_plane_height = new_camera%nx / new_camera%ny * image_plane_width

            ! Construct a basis
            new_camera%b = Basis(                &
                new_camera%look_at - position,   &
                (/ 0.0, 1.0, 0.0/)               &
            )
        end function

        type(Ray) function compute_ray(self, x, y)
            class(Camera), intent(inout) :: self
            integer, intent(in) :: x
            integer, intent(in) :: y
            real :: l, r, b, t, u, v

            l = -self%image_plane_width / 2.0
            r = self%image_plane_width / 2.0
            b = -self%image_plane_height / 2.0
            t = self%image_plane_height / 2.0

            u = l + (r - l) * x / self%nx
            v = b + (t - b) * y / self%ny

            compute_ray = Ray(                                                 &
                self%position,                                                 &
                self%b%w * (-self%focal_length) + self%b%u * u + self%b%v * v  &
            )
        end function
end module
