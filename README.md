## UMD Graphics Raytracer Fortran

This is a port of https://bitbucket.org/timrs2998/umd-graphics from C++ to Fortran. See that project for more details

## Compiling

    sudo apt-get install cmake gfortran git
    git clone git@gitlab.com:timrs2998/umd-graphics-fortran.git
    cd umd-graphics-fortran

    mkdir build
    cd build/
    cmake ..
    make

    ./src/raytracer
    xdg-open image_output.ppm
